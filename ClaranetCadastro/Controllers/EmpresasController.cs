﻿using ClaranetCadastro.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ClaranetCadastro.Controllers
{
    public class EmpresasController : Controller
    {
        //     private DBCt Context { get; }

        //     public EmpresasController(DBCt _context)
        //     {
        //_context = Context;
        //     }

        public IActionResult IndexEmpresas()
        {
            List<Empresas> empresas = new List<Empresas>();

            //using (var dataContext = new DBCt())
            //{
            //	empresas = dataContext.Empresas.ToList();
            //}
            return View(empresas);
        }

        public IActionResult CadastroEmpresas(int? IdEmpresas)
        {
            var empresas = new Empresas();
            if (IdEmpresas != null)
            {
                //using (var dataContext = new DBCt())
                //{
                //	empresas = dataContext.Empresas.Find(x => x.IdEmpresas == IdEmpresas).FirstOrDefault();
                //}
            }
            return View();
        }

        public IActionResult Cadastro(Empresas cadEmpresa)
        {
            var mensagem = string.Empty;

            if (String.IsNullOrEmpty(cadEmpresa.RazaoSocial) || String.IsNullOrEmpty(cadEmpresa.NomeFantasia) || String.IsNullOrEmpty(cadEmpresa.CNPJ))
                mensagem += "Preencha o todos os dados básicos\n";
            else if (ValidaCnpj(cadEmpresa.CNPJ))
                mensagem += "CNPJ Inválido\n";

            if (String.IsNullOrEmpty(cadEmpresa.Email))
                mensagem += "Preencha o campo de e-mail\n";

            if (String.IsNullOrEmpty(cadEmpresa.NomeContato))
                mensagem += "Preencha o nome do contato\n";

            if (String.IsNullOrEmpty(cadEmpresa.Telefone) && String.IsNullOrEmpty(cadEmpresa.TelefoneComercial) && String.IsNullOrEmpty(cadEmpresa.Celular))
                mensagem += "Preencha ao menos um número para contato\n";

            if (String.IsNullOrEmpty(cadEmpresa.CEP))
                mensagem += "Preencha o campo de CEP\n";

            if (String.IsNullOrEmpty(mensagem))
                return RedirectToAction("CadastroEmpresas", cadEmpresa);
            else
            {
                try
                {
                    //using (var dataContext = new DBCt())
                    //{
                    //	dataContext.Empresas.Add(cadEmpresa);
                    //  dataContext.SaveChanges();
                    //}

                }
                catch (Exception ex)
                {

                }

                return RedirectToAction("IndexEmpresas");
            }
        }

        public static bool ValidaCnpj(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;

            cnpj = cnpj.Trim();
            cnpj = cnpj.Replace(".", "").Replace("-", "").Replace("/", "");

            if (cnpj.Length != 14)
                return false;

            tempCnpj = cnpj.Substring(0, 12);

            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];

            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            return cnpj.EndsWith(digito);
        }

        public IActionResult ExcluiCadastroEmpresas(int IdEmpresas)
        {
            try
            {
                //using (var dataContext = new DBCt())
                //{
                //	empresas = dataContext.Empresas.Find(x => x.IdEmpresas == IdEmpresas).FirstOrDefault();
                //}
            }catch(Exception ex)
            {

            }

            return View();
        }
    }
}
