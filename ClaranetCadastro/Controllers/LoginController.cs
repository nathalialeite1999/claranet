﻿using Microsoft.AspNetCore.Mvc;

namespace ClaranetCadastro.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Login()
        {
            return View();
        }

        public IActionResult ValidacaoLogin(Models.Login login)
        {
            return RedirectToAction("Index", "Home");
        }
    }
}