﻿namespace ClaranetCadastro.Models
{
    public class Empresas
    {
        public int IdEmpresas { get; set; }
        public string CNPJ { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string? Telefone { get; set; }
        public string? TelefoneComercial { get; set; }
        public string? Celular { get; set; }
        public string Logradouro { get; set; }
        public int Numero { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CEP { get; set; }
        public string NomeContato { get; set; }
    }
}
