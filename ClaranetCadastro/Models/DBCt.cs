﻿using Microsoft.EntityFrameworkCore;

namespace ClaranetCadastro.Models
{
    public class DBCt : DbContext
    {
        protected readonly IConfiguration Configuration;

        public DBCt(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            // connect to sql server with connection string from app settings
            options.UseSqlServer(Configuration.GetConnectionString("claranet"));
        }
        

        public DbSet<Empresas> Empresas { get; set; }


        protected override void OnModelCreating (ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DBCt).Assembly);
        }
    }
}
